FROM perl:latest as base

LABEL io.opndev.vendor="OPN Development"
LABEL io.opndev.label-with-value="perl"
LABEL version="1.0"
LABEL description="Base image for OPN Development perl projects"
LABEL maintainer="Wesley Schwengle <wesley@opndev.io>"

ONBUILD WORKDIR /tmp/build

ENV NO_NETWORK_TESTING=1 \
    DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get autoremove --purge -yqq\
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/* ~/.cpanm

COPY dev-bin/cpanm /usr/local/bin/docker-cpanm
COPY dev-bin/cpm /usr/local/bin/docker-cpm

# TODO: Move to Moose images only?
RUN docker-cpm --no-test IPC::System::Simple \
    && docker-cpm namespace::autoclean

# Build a development environment
FROM base as development
COPY dev-bin/development dev-bin/development
RUN ./dev-bin/development

FROM base as objectpad
RUN docker-cpm Object::Pad@0.812

FROM base as moo
RUN docker-cpm Moo Types::Standard

FROM development as moo-development
COPY --from=moo /usr/local/lib/perl5/ /usr/local/lib/perl5/

FROM moo as moosy
RUN docker-cpm Moose
RUN docker-cpm MooseX::Extended

FROM development as moosy-development
COPY --from=moo-development /usr/local/lib/perl5/ /usr/local/lib/perl5/
COPY --from=moosy /usr/local/lib/perl5/ /usr/local/lib/perl5/

FROM moosy as mojo
RUN docker-cpm Mojolicious

FROM development as mojo-development
COPY --from=mojo /usr/local/lib/perl5/ /usr/local/lib/perl5/
COPY --from=mojo /usr/local/bin/ /usr/local/bin

FROM base as fat
COPY --from=mojo /usr/local /usr/local
COPY --from=moo /usr/local/lib/perl5/ /usr/local/lib/perl5/
COPY --from=objectpad /usr/local/lib/perl5/ /usr/local/lib/perl5/

FROM development as fat-development
COPY --from=mojo /usr/local /usr/local
COPY --from=moo /usr/local/lib/perl5/ /usr/local/lib/perl5/
COPY --from=objectpad /usr/local/lib/perl5/ /usr/local/lib/perl5/
