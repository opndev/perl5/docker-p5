
set -e

previous=""

# let's assume exit codes, where 0 is OK
local=1
if [ -z "$1" ]
then
  local=0
  mkdir -p local/tmp
  #docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
  #docker buildx create --name multiarch --driver docker-container --use
  #docker buildx inspect --bootstrap
fi

tag=${1:-$(git rev-parse --abbrev-ref HEAD)}
image=${2:-registry.gitlab.com/opndev/perl5/docker-p5}

#BUILDKIT_ARCHS=${BUILDKIT_ARCHS:-linux/amd64,linux/arm64}
BUILDKIT_ARCHS=${BUILDKIT_ARCHS:-linux/amd64}

[ "$tag" = 'master' ] && tag=latest;

_docker_pull() {
  set +e
  docker pull $1 1>/dev/null 2>/dev/null
  local rv=$?
  set -e
  return $rv
}

_get_docker_cache_image() {
  local docker_tag=$1
  local latest=$2
  _docker_pull $docker_tag && echo $docker_tag && return
  _docker_pull $latest && echo $latest && return
}

_build_image() {
    local label=$1

    docker_tag="${image}${label}"
    echo "Building $docker_tag";
    echo ""

    local push_spec="--output=type=registry"
    local tmp="local/tmp/docker-cache-$i"

    if [ $local -eq 0 ]
    then
        push_spec="--output=type=oci,dest=local/$i.oci.tar"
    fi

    local img_cache=$(_get_docker_cache_image $docker_tag)
    previous="$previous --cache-from=$img_cache"
    # buildx pushes to the registry for us
    docker buildx build \
        $DOCKER_OPTS \
        --platform="$BUILDKIT_ARCHS" \
        --pull \
        --cache-to="type=local,dest=$tmp" \
        $previous \
        --target $i \
        --tag "$docker_tag" \
        $push_spec \
        "."
    previous="$previous --cache-from=type=local,src=$tmp"

}

for i in $(grep '^FROM .* as .*' Dockerfile | awk '{print $NF}')
do

    if [ $i = "base" ]
    then
        label=":$tag"
    else
        label="/$i:$tag"
    fi

    _build_image $label

done

# vim: ft=zsh
